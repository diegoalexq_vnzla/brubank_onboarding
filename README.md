# Proyecto creado con Create React App

Este proyecto se inicio con [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

Antes de ejecutar el proyecto tienes que realizar un:

### `npm install`

Y tambien verificar que tengas instalado al menos la version 12 de node

### `node -v`


En el directorio del proyecto se puede ejecutar cualquiera de los dos comandos dependiendo si tienes npm o yarn:

### `yarn start`

### `npm start`


La aplicacion va a arrancar en modo desarrollo y levantara la siguiente URL.\
 [http://localhost:3000](http://localhost:3000).
