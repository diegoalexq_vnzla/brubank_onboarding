import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import getPrices from '../services/getPrices'

// example of json price
// const data =  [
//   { "BTC": 541925.0064359717 },
//   { "ETH": 111709.74823250688 }
// ]

function transformLocaleString(value){
  return value.toLocaleString(undefined, { maximumFractionDigits: 2 })
}

function Bitcoin() {
  let history = useHistory();

  const redirect = () => {
    history.push('/onboarding')
  }
  
  const [money,setMoney] = useState([])

  useEffect(() => {
    getPrices().then( result=> (result) ? setMoney(result) : '')
  },[]);

  return (
  <div className="flex h-screen">
    <div className= "m-auto w-72 m-3">
      <p className="font-normal text-lg">
        BitcoinTracer
      </p>
      {
        money.map((singleMoney) => {
          return <div className="flex items-center ml-5 mt-6 mb-6">
            <div className="w-1/6">
              <img className="h-11 m-auto" src={`/icons/${singleMoney.name}.png`} alt={`${singleMoney.name}`}/>
            </div>
            <div className ="w-5/6 pl-4">
              <h4 className="text-brubank-secondary"> USD {transformLocaleString(singleMoney.value)}</h4>
              <div className="text-gray-400 text-xs"> Precio {singleMoney.code} </div>
            </div>
          </div>
        })
      }
      <button 
        onClick={redirect} 
        className="w-full bg-black hover:bg-gray-800 text-white font-normal py-2 px-4 rounded-lg">
          Hacerme cuenta
      </button>
    </div>
  </div>
  );
}

export default Bitcoin;
