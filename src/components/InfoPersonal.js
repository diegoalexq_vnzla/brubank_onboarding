import React from 'react'

import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";


function InfoPersonal(props) {

  const InfoPersonalSchema = Yup.object({
    dni: Yup.number().required('El número de documento es requerido').label("Documento").typeError('solo se permiten caracteres numéricos'),
    email: Yup.string().email('Formato de correo inválido').required('El correo electrónico es requerido').label('Correo electrónico'),
    name: Yup.string().required('El nombre es requerido').label("Nombre"),
    last_name: Yup.string().required('El apellido es requerido').label("Apellido"),
    birth_date: Yup.string().required('La fecha de nacimiento es requerida').label("Fecha de nacimiento")
  });
  
  const handleSubmit = (values) => {
    props.next(values);
  };
  
  return (
    <div className="flex h-screen m-4">
      <div className="w-96 m-auto border border-brubank-border rounded-2xl">
        <div className="mr-5 ml-5 mt-5 bg-brubank-gray rounded-2xl h-40 border border-brubank-border">
          <img className="h-14 m-auto mt-10" src="/icons/user.png" alt="image"/>
          <h4 className="text-center mt-2 font-bold text-lg">Crear Cuenta</h4>
        </div>
        <Formik
          validationSchema={InfoPersonalSchema}
          initialValues={props.data}
          onSubmit={handleSubmit}
        >
          {() => (
            <Form className="p-5">
              <div className="flex">
                <div className="w-1/6">
                  <p className="text-xs">Tipo</p>
                  <Field className="font-semibold shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                    name="dni_type"
                  />

                </div>
                <div className="w-5/6 pl-4">
                  <p className="text-xs">Documento</p>
                  <Field className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                    name="dni"
                    placeholder="000.000.000"
                  />
                  <ErrorMessage component="div"  className="pt-1 text-xs text-red-400" name="dni" />
                </div>
              </div>
              <p className="text-xs mt-3">Correo electrónico</p>
              <Field className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                type="email" 
                name="email" 
                placeholder="ejemplo@ejemplo.com"
              />
              <ErrorMessage component="div" className="pt-1 text-xs text-red-400" name="email" />
              <p className="text-xs mt-3">Nombre</p>
              <Field className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                name="name" 
                placeholder="Ingresa tu nombre"
              />
              <ErrorMessage component="div" className="pt-1 text-xs text-red-400" name="name" />
              <p className="text-xs mt-3">Apellido</p>
              <Field className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                name="last_name"
                placeholder="Ingresa tu apellido"
              />
              <ErrorMessage component="div" className="pt-1 text-xs text-red-400" name="last_name" />
              <p className="text-xs mt-3">Fecha de nacimiento</p>
              <Field className="w-1/2 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                name="birth_date" 
                placeholder="24/09/1945"
              />
              <ErrorMessage component="div" className="pt-1 text-xs text-red-400" name="birth_date" />
              <button 
                type="submit" 
                className="w-full text-white mt-5 bg-brubank-purple font-normal py-2 px-4 rounded"
              >
                Siguiente
              </button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}

export default InfoPersonal