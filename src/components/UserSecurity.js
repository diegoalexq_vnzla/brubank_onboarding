import React, { useState } from 'react';

import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'

const Alert = () => {
  return (
    <div className="mb-5 bg-brubank-red text-white px-4 py-3 rounded-xl relative" role="alert">
      <div className="flex">
        <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M13.1333 24.6689C14.826 24.6689 16.4171 24.3452 17.9067 23.6978C19.3963 23.0503 20.7082 22.1595 21.8423 21.0254C22.9764 19.8913 23.8651 18.5815 24.5083 17.0962C25.1515 15.6108 25.4731 14.0218 25.4731 12.3291C25.4731 10.6364 25.1515 9.04736 24.5083 7.56201C23.8651 6.07666 22.9743 4.76481 21.8359 3.62646C20.6976 2.48812 19.3836 1.59733 17.894 0.954102C16.4045 0.310872 14.8133 -0.0107422 13.1206 -0.0107422C11.4448 -0.0107422 9.86214 0.310872 8.37256 0.954102C6.88298 1.59733 5.56901 2.48812 4.43066 3.62646C3.29232 4.76481 2.40153 6.07666 1.7583 7.56201C1.11507 9.04736 0.793457 10.6364 0.793457 12.3291C0.793457 14.0218 1.11719 15.6108 1.76465 17.0962C2.41211 18.5815 3.3029 19.8913 4.43701 21.0254C5.57113 22.1595 6.88298 23.0503 8.37256 23.6978C9.86214 24.3452 11.4491 24.6689 13.1333 24.6689ZM13.1333 14.4619C12.4393 14.4619 12.0838 14.1107 12.0669 13.4082L11.9019 7.16211C11.8849 6.82357 11.9907 6.54215 12.2192 6.31787C12.4477 6.09359 12.7482 5.98145 13.1206 5.98145C13.4845 5.98145 13.7808 6.09359 14.0093 6.31787C14.2378 6.54215 14.3521 6.8278 14.3521 7.1748L14.1743 13.4082C14.1574 14.1107 13.8104 14.4619 13.1333 14.4619ZM13.1333 18.5625C12.744 18.5625 12.4097 18.4334 12.1304 18.1753C11.8511 17.9172 11.7114 17.6019 11.7114 17.2295C11.7114 16.8486 11.8511 16.5291 12.1304 16.271C12.4097 16.0129 12.744 15.8838 13.1333 15.8838C13.5226 15.8838 13.8569 16.0129 14.1362 16.271C14.4155 16.5291 14.5552 16.8486 14.5552 17.2295C14.5552 17.6104 14.4134 17.9277 14.1299 18.1816C13.8464 18.4355 13.5142 18.5625 13.1333 18.5625Z" fill="white"/>
        </svg>
        <span className="block sm:inline ml-3">Las claves no coinciden</span>
      </div>
    </div>
  )
};

function UserSecurity(props) {
  const [showError, setshowError] = useState(false)
  const [values, setValues] = useState({
    showPassword: false,
    showPasswordConfirmation:false
  });
  
  const handleSubmit = (values) => {
    if(values.password !== values.passwordConfirmation) {
      setshowError(true)
    }else{
      setshowError(false)
      props.next(values);
    }
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };
  const handleClickShowPasswordConfirmation = () => {
    setValues({ ...values, showPasswordConfirmation: !values.showPasswordConfirmation });
  };

  const UserSecuritySchema = Yup.object({
    password: Yup.string().required('Ingresa tu clave').matches(/((?=.*\d)(?=.*[A-Z]).{8,8})/,"Debe contener 8 caracteres en total, una mayúscula, una minúscula, un número"),
    passwordConfirmation: Yup.string().required('Ingresa tu confirmacion de clave').matches(/((?=.*\d)(?=.*[A-Z]).{8,8})/,"Debe contener 8 caracteres en total, una mayúscula, una minúscula, un número")
  });
  
  return (
    <div className="flex h-screen m-4">
      <div className="w-96 m-auto">
        { showError ? <Alert /> : null }
        <div className="w-96 m-auto border border-brubank-border rounded-2xl">
          <div className="mr-5 ml-5 mt-4 bg-brubank-gray rounded-2xl h-40 border border-brubank-border">
            <img className="h-16 m-auto mt-8" src="/icons/key.png" alt="image" />
            <h4 className="text-center mt-1 font-bold text-lg">Seguridad</h4>
          </div>
          {/* <pre>{JSON.stringify(showError)}</pre> */}
          <Formik
            validationSchema={UserSecuritySchema}
            initialValues={props.data}
            onSubmit={handleSubmit}
          >
            {() => (
              <Form className="p-5">
                <p className="text-xs mt-3">Elegi tu clave</p>
                <label className="relative">
                  <Field 
                    className={showError ? 'shadow appearance-none border border-brubank-red rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline' : 
                    'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'} 
                    name="password" 
                    type={values.showPassword ? "text" : "password"}
                  />
                  <FontAwesomeIcon icon={faEye} onClick={handleClickShowPassword} className=" w-8 h-8 absolute top-1/2 transform -translate-y-1/2 right-3 cursor-pointer" />
                </label>
                <ErrorMessage component="div" className="pt-1 text-xs text-red-400" name="password" />

                <p className="text-xs mt-3">Confirma tu clave</p>
                <label className="relative">
                  <Field 
                    className={showError ? 'shadow appearance-none border border-brubank-red rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline' : 
                    'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'} 
                    name="passwordConfirmation"
                    type={values.showPasswordConfirmation ? "text" : "password"}
                  />
                  <FontAwesomeIcon icon={faEye} onClick={handleClickShowPasswordConfirmation} className=" w-8 h-8 absolute top-1/2 transform -translate-y-1/2 right-3 cursor-pointer" />
                </label>
                <ErrorMessage component="div" className="pt-1 text-xs text-red-400" name="passwordConfirmation" />
                <button 
                  type="submit" 
                  className="w-full text-white mt-5 bg-brubank-purple font-normal py-2 px-4 rounded"
                >
                  Siguiente
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}

export default UserSecurity