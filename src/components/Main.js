import React, { useState } from 'react'

import StepOne from './InfoPersonal'
import StepTwo from './UploadDocument'
import StepThree from './UserSecurity'
import StepFour from './SendInfo'


function MainOnboarding() {
  const [currentStep, setCurrentStep] = useState(0);

  /*El estado de la fecha es fijo 
    ya que no agregare ningun plugin de calendar, 
    ni manejare fecha. Me enfoco en la funcionalidad en general
  */
  const [data, setData] = useState({
    dni_type: "DNI",
    dni: "",
    email: "",
    name: "",
    last_name: "",
    password: "",
    birth_date: "2002-08-04",
    'files[]': '',
  });

  const handleNextStep = (newData) => {
    setData((prev) => ({ ...prev, ...newData }));
    setCurrentStep((prev) => prev + 1);
  };

  const steps = [
    <StepOne next={handleNextStep} data={data} />,
    <StepTwo next={handleNextStep}  data={data} />,
    <StepThree next={handleNextStep}  data={data} />,
    <StepFour next={handleNextStep}  data={data} />
  ];

  return <div className="App">{steps[currentStep]}</div>;
}

export default MainOnboarding
