import axios from 'axios'

function postSignup(props) {
    const formData = new FormData();

    formData.append('name', props.data.name);
    formData.append('last_name', props.data.last_name);
    formData.append('birth_date', props.data.birth_date);
    formData.append('dni', props.data.dni);
    formData.append('dni_type', props.data.dni_type);
    formData.append('email', props.data.email);
    formData.append('password', props.data.password);
    formData.append('files[]', props.data.file_1);
    formData.append('files[]', props.data.file_2);

    const baseUrl = 'https://interview-front-api.herokuapp.com/'

    return axios.post(`${baseUrl}signup`,formData)
    .then((result) => {
      return result
    })
    .catch((error) => {
      return error;
    });
}

export default postSignup