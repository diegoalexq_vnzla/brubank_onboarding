import axios from 'axios'


function getPrices() {

    const baseUrl = 'https://interview-front-api.herokuapp.com/'

    return axios.get(`${baseUrl}prices`)
    .then((result) => {
      let { data } = result
      data.map( money => {
        if(money.BTC) {
          return (money.code ='BTC', money.name = 'Bitcoin',money.value = money.BTC)
        }
        if(money.ETH) {
          return (money.code ='ETH', money.name = 'Etherium',money.value = money.ETH)
        }
      })
      return data
    })
    .catch((error) => {
      return error;
    });

}

export default getPrices