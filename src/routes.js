import React from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import InfoBitcoin from './components/InfoBitcoin' 
import Main from './components/Main' 

const routing = () => 
  <Router>
    <Route exact path="/" component={InfoBitcoin}/>
    <Route exact path="/onboarding" component={Main}/>
  </Router>

export default routing