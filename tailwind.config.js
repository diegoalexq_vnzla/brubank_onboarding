
const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      display: ["group-hover"],
      colors: {
        'brubank-secondary': '#262626',
        'brubank-gray': '#F1F1F1',
        'brubank-border': '#EBEBEB',
        'brubank-purple' :'#614AD9',
        'brubank-red' :'#FF1400',
        'brubank-green' :'#2DCA73',
        'brubank-icon-red' :'#E83B49',
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
